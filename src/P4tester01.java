import java.util.Arrays;

public class P4tester01 {

    public static void main(String[] args) {
        Punkt4[] ps = new Punkt4[8];
        int ft1 = 1;
        int ft2 = 1;
        for(int i = 0; i<ps.length; i++) {
            if (i % 8 == 4) { ft1 *= -1; }
            if (i % 4 == 2) { ft2 *= -1; }
            ps[i] = new Punkt4(ft1 * (((i+1) % 4)/2 + 1), ft2 * (((i+3) % 4)/2 +1));
        }
        System.out.println(Arrays.toString(ps));
        Arrays.sort(ps);
        System.out.println(Arrays.toString(ps));
    }
}