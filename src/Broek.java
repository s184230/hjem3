// Broek klasse, som kan reducere
// plusse, gange, minus og tage den
// reciprokke vaerdi af broeker
public class Broek {
	
	// long-v�rdier for at undg� (udskyde)
	// hvorn�r overflow sker.
	// final for at de ikke kan aendres n�r
	// de f�rst er constructed
	private final long t, n;

	
	// Constructor der tager int som input
	public Broek(int t, int n) {
		if (n == 0) {
			throw new IllegalArgumentException("n != 0");
		} else if (n<0) {
			this.t = t * (-1);
			this.n = n * (-1);
		} else {
		this.t = (long) t;
		this.n = (long) n;
		}
	}
	
	// Constructor som tager longs som input.
	// hvis to broeker hver med en vaerdi paa f.eks
	// Integer.MAX_VALUE ligges sammen sker der ikke
	// overflow
	public Broek(long t, long n) {
		if (n == 0) {
			throw new IllegalArgumentException("n != 0");
		} else if (n<0) {
			this.t = t * (-1);
			this.n = n * (-1);
		} else {
		this.t = t;
		this.n = n;
		}
	}

	// plus metode. Faellesnaevner findes altid, men
	// broeken der returneres er reduceret mest muligt
	// s� den er s� p�n og simpel som mulig
	public Broek plus(Broek f) {
		if (f == null) {
			return new Broek(this.t, this.n);
		}

		long t = (this.t * f.n) + (f.t * this.n);
		long n = this.n * f.n;

		return simplify(new Broek(t, n));
	}

	// genanvender plus metoden
	public Broek minus(Broek f) {
		return simplify(plus(new Broek(-f.t, f.n)));
	}

	public Broek gange(Broek f) {
		if (f == null) {
			return new Broek(this.t, this.n);
		}
		long t = this.t * f.t;
		long n = this.n * f.n;
		return simplify(new Broek(t, n));
	}

	public Broek reciprok() {
		return simplify(new Broek(this.n, this.t));
	}

	// returnere et heltal hvis broeken svarer til et heltal
	// (taeller modulus naevner giver 0)
	public String toString() {
		if (this.t % this.n == 0) return "" + (this.t / this.n);

		return t + "/" + n;
	}

	// stoerste faelles divisor (gcd)
	// finder stoerste faelles divisor
	// private da den kun boer bruges
	// internt i denne klasse
	private long sfd(long a, long b) {
		if (b == 0) {
			return a;
		}
		return sfd(b, a % b);
	}

	// returnerer broeken s reduceret
	public Broek simplify(Broek s) {
		long sfd = sfd(s.t, s.n);

		if (sfd == 1) {
			return new Broek(s.t, s.n);
		} else {
			return new Broek(s.t / sfd, s.n / sfd);
		}
	}
}