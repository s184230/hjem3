@SuppressWarnings("serial")
public class Punkt1 extends Punkt implements Comparable<Punkt1> {

    // Punkt1 constructor jeg kalder Punkt constructor da felterne bliver de samme
    public Punkt1(int x, int y) {
        super(x, y);
    }

    public int compareTo(Punkt1 o) {
        if (this.getX() < o.getX()) { //tjekker om x er mindre end x′
            return (int) (this.getX() - o.getX());
        } else { // eller y er mindre end y′
            return (int) (this.getY() - o.getY());
        }
    }


}