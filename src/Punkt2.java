@SuppressWarnings("serial")
public class Punkt2 extends Punkt implements Comparable<Punkt2> {

	
	// Punkt2 constructor - kalder bare Point's constructor da felterne er de samme
	public Punkt2(int x, int y) {
		super(x, y);
	}

	// compareTo for Punkt2
	public int compareTo(Punkt2 p) {

		// hvis x+y for de to punkter ikke er ens, returneres forskellen mellem de to
		// denne m� v�re negativ, hvis instansen er er mindre end p
		if (p.x + p.y != this.x + this.y) {
			return (this.x + this.y) - (p.x + p.y);
			
		} else // ellers m� summen v�re ens, og differensen sammenlignes efter samme princip 
			return (this.y - this.x) - (p.y - p.x);
		}
}
