@SuppressWarnings("serial")
public class Punkt4 extends Punkt implements Comparable<Punkt4>{
	
	// gemmer instansens afstand til origo, samt dens vinkel
	// Math.atan2 returnerer vinklen mellem Pi -> -Pi, så denne
	// konverteres først til grader (180 -> -180), og dette så
	// til intervallet (0 -> 360)
	private double dist = Math.sqrt(this.x*this.x + this.y*this.y);
	private double angle = (Math.toDegrees(Math.atan2(this.y, this.x)) + 360) % 360;

	public Punkt4(int x, int y) {
		super(x,y);
	}
	
	public int compareTo(Punkt4 p) {
		if (this.dist != p.dist) {
			return (int) Math.signum(this.dist - p.dist);
		} else {
			return (int) Math.signum(this.angle - p.angle);
		}
	}
	
	public double getDist() {
		return this.dist;
	}
	
	public double getAngle() {
		return this.angle;
	}
	
	
}
