public class Gennemsnit {
	
	// beregner gennemsnit af et int[] array
	// returnerer gennemsnittet - eller NaN (not a number)
	// hvis arrayet ikke har nogle elementer eller er null
	public static double beregn(int[] tal) {
		if (tal == null) {
			return Double.NaN;
		}
		
		// Long sum-v�rdi for at haandtere st�rre tal
		long sum = 0L;
		
		// for-each loop som finder summen af arrayet
		for (int num : tal) sum+=num;
		
		// return gennemsnittet (castes til double da b�de sum og .length ikke er floats)
		return (double) sum / tal.length;
	}
}
