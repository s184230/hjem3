import java.util.Arrays;

public class P3tester01 {

    public static void main(String[] args) {
        Punkt3[] ps = new Punkt3[8];
        for(int i = 0; i<ps.length; i++) {
            ps[i] = new Punkt3(i,0);
        }
        System.out.println(Arrays.toString(ps));
        Arrays.sort(ps);
        System.out.println(Arrays.toString(ps));
    }
}
