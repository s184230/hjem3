import java.awt.*;

// klasse som der holder toString funktionen for de tre punkter
@SuppressWarnings("serial")
public class Punkt extends Point {

    public Punkt(int x, int y) {
        super(x,y);
    }

    public String toString() {
        return "(" + this.x + "," + this.y + ")";
    }
}
