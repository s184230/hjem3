@SuppressWarnings ("serial")
public class Punkt3 extends Punkt implements Comparable<Punkt3> {

    // Punkt3 constructor - referer til Point's constructor
    public Punkt3 (int x, int y) {
        super(x,y);
    }

    // compareTo for Punkt3
    public int compareTo(Punkt3 o) {

        // Tjekker første betingelse (y mindre end y')
        if ((o.getY() != this.getY()) && this.y < o.getY()) {
            return 1;
        } else if (this.y == o.getY() && o.getX() < this.getX()) { // Anden betingelse (y==y' og x' < x)
            return -1;
        } else { // Hvis ingen af de to ovenstående betingelser er opfyldt, sker der ingen ordning.
            return 0;
        }

    }
}
